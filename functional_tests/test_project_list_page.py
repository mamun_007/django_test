from selenium import webdriver
from budget.models import Project
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys
from django.urls import reverse
import time


class TestProjectListPage(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver')

    def tearDown(self):
        self.browser.close() 

        
    def test_no_projects_alert_is_displayed(self):
        self.browser.get(self.live_server_url) 
        time.sleep(10)

        #The user request!!

        alert = self.browser.find_element_by_class_name('noproject-wrapper')
        self.assertEquals(
            alert.find_element_by_tag_name('h3').text,
            'Sorry, you don\'t have any projects, yet.'
          )
    
    def test_no_projects_alert_button_works(self):
        self.browser.get(self.live_server_url)
        time.sleep(3)

        #The user requests & url redection is ok!!

        add_url = self.live_server_url  + reverse('add')
        self.browser.find_element_by_tag_name('a').click()
        self.assertEquals(
            self.browser.current_url , add_url
        ) 

    def test_user_sees_project_list(self):
        project1 = Project.objects.create(
            name = 'project1',
            budget = 100000
        )
        self.browser.get(self.live_server_url)
        time.sleep(3)

        #The user sees project!!

        self.assertEquals(
            self.browser.find_element_by_tag_name('h5').text,
             'project1'
        )


    def test_user_is_redirected_to_project_details(self):
        project1 = Project.objects.create(
            name = 'project1',
            budget = 100000
        )
        self.browser.get(self.live_server_url)
        time.sleep(3)

        #The user sees the project on the screen. He clicks the 'visit' link
        #and is redirected to project details

        detail_url = self.live_server_url + reverse('detail', args=[project1.slug])
        self.browser.find_element_by_link_text('VISIT').click()
        self.assertEquals(
            self.browser.current_url, detail_url
        )

        
    def test_create_project_by_sending_keys(self):

        self.browser.get(self.live_server_url + reverse('add'))
        time.sleep(3)

        #Send keys to create project

        element_name = self.browser.find_element_by_name('name')
        element_name.clear()
        element_name.send_keys("Project2")

        element_budget = self.browser.find_element_by_name('budget')
        element_budget.clear()
        element_budget.send_keys("10000")


        element_category = self.browser.find_element_by_name('categoryInput')
        element_category.clear()
        element_category.send_keys("Development")
        
        element_button = self.browser.find_element_by_class_name('btn')
        element_button.click()

        detail_url = self.live_server_url + '/project2/'
        time.sleep(5)


        self.assertEquals(
            self.browser.current_url, detail_url
        )





